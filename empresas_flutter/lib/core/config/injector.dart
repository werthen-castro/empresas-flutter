import 'package:empresas_flutter/core/http_client/custom_http_client.dart';
import 'package:empresas_flutter/core/http_client/custom_http_client_impl.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source_impl.dart';
import 'package:empresas_flutter/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:empresas_flutter/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:empresas_flutter/features/authentication/domain/usecases/sign_in_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/data/data_source/get_enterprise_data_source.dart';
import 'package:empresas_flutter/features/get_enterprise/data/data_source/get_enterprise_data_source_impl.dart';
import 'package:empresas_flutter/features/get_enterprise/data/repositories/get_enterprises_repository_impl.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/repositories/get_enterprises_repository.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_for_id_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_search_name_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_usecase.dart';
import 'package:get_it/get_it.dart';

class Injector {
  static GetIt getIt = GetIt.instance;

  static void init() async {
    // Client rest
    getIt.registerLazySingleton<CustomHttpClient>(() => CustomHttpClientImpl());

    // ---------------- Feature Authentication  -----------------------
    // Remote datasources
    getIt.registerFactory<AuthenticationDataSource>(() => AuthenticationDataSourceImpl(getIt.get<CustomHttpClient>()));

    // Repository
    getIt.registerFactory<AuthenticationRepository>(
        () => AuthenticationRepositoryImpl(getIt.get<AuthenticationDataSource>()));

    // Usecase
    getIt.registerLazySingleton(() => AuthenticationUseCase(getIt.get<AuthenticationRepository>()));

    // ---------------- Feature Get Enterprises  -----------------------
    // Remote datasources
    getIt.registerFactory<GetEnterprisesDataSource>(() => GetEnterprisesDataSourceImpl(getIt.get<CustomHttpClient>()));

    // Repository
    getIt.registerFactory<GetEnterprisesRepository>(
        () => GetEnterprisesRepositoryImpl(getIt.get<GetEnterprisesDataSource>()));

    // Usecases
    getIt.registerLazySingleton(() => GetEnterprisesUseCase(getIt.get<GetEnterprisesRepository>()));

    getIt.registerLazySingleton(() => GetEnterprisesSearchNameUseCase(getIt.get<GetEnterprisesRepository>()));

    getIt.registerLazySingleton(() => GetEnterpriseForIdUseCase(getIt.get<GetEnterprisesRepository>()));
  }
}
