import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:empresas_flutter/core/erros/error_messages.dart';
import 'package:empresas_flutter/core/utils/secure_storage.dart';
import 'package:flutter/material.dart';
import 'custom_http_client.dart';
import 'custom_http_response.dart';

class CustomHttpClientImpl implements CustomHttpClient, InterceptorsWrapper {
  final Dio _dio = Dio();

  CustomHttpClientImpl() {
    _dio.interceptors.add(this);
  }

  @override
  Future<CustomHttpResponse> get(String url) async {
    Response response;

    try {
      response = await _dio.get(url);
    } on DioError catch (error) {
      return CustomHttpResponse(
        statusCode: error.response?.statusCode,
        statusMessage: error.response?.statusMessage,
        apiError: error.response?.data["errors"],
      );
    } on TimeoutException catch (_) {
      return CustomHttpResponse(
        statusMessage: ErrorMessages.errorTimeout,
        timeout: true,
      );
    } on SocketException catch (exception) {
      return CustomHttpResponse(
        statusMessage: exception.message,
      );
    }

    return CustomHttpResponse(
      data: response.data,
      statusCode: response.statusCode,
    );
  }

  @override
  Future<CustomHttpResponse> post(String url, {Map<String, dynamic>? body}) async {
    Response response;

    try {
      response = await _dio.post(url, data: body);
    } on DioError catch (error) {
      return CustomHttpResponse(
        statusCode: error.response?.statusCode,
        statusMessage: error.response?.statusMessage,
        apiError: error.response?.data["errors"].first,
      );
    } on TimeoutException catch (_) {
      return CustomHttpResponse(
        statusMessage: ErrorMessages.errorTimeout,
        timeout: true,
      );
    } on SocketException catch (exception) {
      return CustomHttpResponse(
        statusMessage: exception.message,
      );
    }

    return CustomHttpResponse(
      data: response.data,
      statusCode: response.statusCode,
      header: response.headers.map,
    );
  }

  _getHeaders() async {
    SecureStorage secureStorage = SecureStorage();
    String? token = await secureStorage.getValue("access-token");
    String? client = await secureStorage.getValue("client");
    String? uid = await secureStorage.getValue("uid");
    Map<String, dynamic> headers = {"access-token": token, "client": client, "uid": uid};
    return headers;
  }

  @override
  void onError(DioError error, ErrorInterceptorHandler handler) {
    debugPrint(error.message);
    debugPrint(error.response.toString());
    handler.next(error);
  }

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    options.headers = await _getHeaders();
    handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    debugPrint('\n----------------------------------------------------------------------------------------');
    debugPrint(response.realUri.toString());
    debugPrint(response.statusCode.toString());
    debugPrint('----------------------------------------------------------------------------------------\n');
    handler.next(response);
  }
}
