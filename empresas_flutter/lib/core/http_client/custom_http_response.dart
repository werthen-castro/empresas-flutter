class CustomHttpResponse {
  final dynamic data;
  final int? statusCode;
  final String? statusMessage;
  final String? apiError;
  final bool timeout;
  final bool sucess;
  final Map<String, dynamic> header;

  CustomHttpResponse({
    this.data,
    this.statusCode,
    this.statusMessage,
    this.apiError,
    this.timeout = false,
    this.sucess = false,
    this.header = const {},
  });
}
