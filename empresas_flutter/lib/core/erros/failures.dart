import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final String? errorMessage;
  final int? statusCode;
  final bool timeout;

  const Failure({this.errorMessage, this.statusCode, this.timeout = false});
}

class ServerFailure extends Failure {
  final String? errorMessage;
  final int? statusCode;
  final bool timeout;

  const ServerFailure({this.errorMessage, this.statusCode, this.timeout = false});

  @override
  List<Object?> get props => [];
}

class NullParamsFailure extends Failure {
  @override
  List<Object?> get props => [];
}
