import 'package:equatable/equatable.dart';

class CustomServerException extends Equatable implements Exception {
  final String? messageError;
  final bool timeout;
  final String? apiError;
  final int? statusCode;

  const CustomServerException({this.messageError, this.timeout = false, this.apiError, this.statusCode});

  @override
  List<Object?> get props => [];
}
