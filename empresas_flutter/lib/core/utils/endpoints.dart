class Endpoints {
  static String urlBase = 'https://empresas.ioasys.com.br';

  static String signIn() => '$urlBase/api/v1/users/auth/sign_in';

  static String getEnterprises() => '$urlBase/api/v1/enterprises';

  static String getEnterprisesForId(int id) => '$urlBase/api/v1/enterprises/$id';

  static String getSearchEnterprises(String name) => '$urlBase/api/v1/enterprises?name=$name';

  static String getImage(int id) => '$urlBase/uploads/enterprise/photo/$id/240.jpeg';
}
