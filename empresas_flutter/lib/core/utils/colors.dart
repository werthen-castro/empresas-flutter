import 'package:flutter/material.dart';

class AppColors {
  static Color primaryColor = const Color(0xFFE01E69);
  static Color backgroundColorInput = const Color(0xFFF5F5F5);
  static Color errorColor = const Color(0xFFEB5757);
}
