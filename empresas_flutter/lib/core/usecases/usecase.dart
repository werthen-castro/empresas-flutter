import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:equatable/equatable.dart';

abstract class Usecase<Output, Input> {
  Future<Either<Failure, Output>> call(Input params);
}

class NoParams extends Equatable {
  @override
  List<Object> get props => [];
}

class ListParams extends Equatable {
  final String email;
  final String password;

  ListParams({
    required this.email,
    required this.password,
  });

  @override
  List<Object?> get props => [email, password];
}
