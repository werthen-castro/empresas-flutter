import 'package:empresas_flutter/core/config/injector.dart';
import 'package:empresas_flutter/features/authentication/presentation/pages/sign_in_page.dart';
import 'package:flutter/material.dart';

void main() {
  Injector.init();

  runApp(MaterialApp(
    home: SignInPage(),
  ));
}
