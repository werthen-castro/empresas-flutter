import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/features/authentication/data/models/auth_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/sign_in_entity.dart';
import 'package:empresas_flutter/features/authentication/domain/repositories/authentication_repository.dart';

class AuthenticationUseCase implements Usecase<SignInEntity, ListParams> {
  final AuthenticationRepository repository;

  AuthenticationUseCase(this.repository);

  @override
  Future<Either<Failure, SignInEntity>> call(ListParams params) async {
    return repository.signIn(auth: AuthModel(email: params.email, password: params.password));
  }
}
