import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

import 'package:empresas_flutter/features/authentication/domain/entities/investor_entity.dart';

class SignInEntity extends Equatable {
  late InvestorEntity investor;
  late dynamic enterprise;
  late bool success;
  late Map<String, dynamic> headers;

  SignInEntity({
    required this.investor,
    required this.enterprise,
    required this.success,
    required this.headers,
  });

  @override
  List<Object?> get props => [investor, enterprise, success, headers];
}
