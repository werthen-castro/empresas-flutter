import 'package:equatable/equatable.dart';

import 'portfolio_entity.dart';

class InvestorEntity extends Equatable {
  late int id;
  late String investorName;
  late String email;
  late String city;
  late String country;
  late double balance;
  late String? photo;
  late PortfolioEntity portfolio;
  late double portfolioValue;
  late bool firstAccess;
  late bool superAngel;

  InvestorEntity({
    required this.id,
    required this.investorName,
    required this.email,
    required this.city,
    required this.country,
    required this.balance,
    this.photo,
    required this.portfolio,
    required this.portfolioValue,
    required this.firstAccess,
    required this.superAngel,
  });

  @override
  List<Object?> get props =>
      [id, investorName, email, city, country, balance, photo, portfolio, portfolioValue, firstAccess, superAngel];
}
