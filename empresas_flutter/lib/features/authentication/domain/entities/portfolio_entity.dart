import 'package:equatable/equatable.dart';

class PortfolioEntity extends Equatable {
  late int enterprisesNumber;
  late List enterprises;

  PortfolioEntity({
    required this.enterprisesNumber,
    required this.enterprises,
  });

  @override
  List<Object?> get props => [enterprisesNumber, enterprises];
}
