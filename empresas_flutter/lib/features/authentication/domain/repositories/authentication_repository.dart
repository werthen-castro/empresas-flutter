import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/features/authentication/data/models/auth_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/sign_in_entity.dart';

abstract class AuthenticationRepository {
  Future<Either<Failure, SignInEntity>> signIn({required AuthModel auth});
}
