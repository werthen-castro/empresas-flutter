import 'package:empresas_flutter/features/authentication/domain/entities/investor_entity.dart';

import 'portfolio_model.dart';

class InvestorModel extends InvestorEntity {
  InvestorModel({
    required int id,
    required String investorName,
    required String email,
    required String country,
    required String city,
    required double balance,
    String? photo,
    required PortfolioModel portfolio,
    required double portfolioValue,
    required bool firstAccess,
    required bool superAngel,
  }) : super(
            id: id,
            investorName: investorName,
            email: email,
            city: city,
            balance: balance,
            country: country,
            portfolioValue: portfolioValue,
            photo: photo,
            portfolio: portfolio,
            firstAccess: firstAccess,
            superAngel: superAngel);

  factory InvestorModel.fromMap(Map<String, dynamic> map) {
    return InvestorModel(
      id: map["id"],
      investorName: map["investor_name"],
      email: map["email"],
      city: map["city"],
      country: map["country"],
      balance: map["balance"],
      portfolioValue: map["portfolio_value"],
      photo: map["photo"],
      portfolio: PortfolioModel.fromMap(map["portfolio"]),
      firstAccess: map["first_access"],
      superAngel: map["super_angel"],
    );
  }
}
