import 'package:empresas_flutter/core/http_client/custom_http_response.dart';
import 'package:empresas_flutter/features/authentication/data/models/investor_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/sign_in_entity.dart';

class SingInModel extends SignInEntity {
  SingInModel(
      {required InvestorModel investor,
      required dynamic enterprise,
      required bool success,
      required Map<String, dynamic> headers})
      : super(investor: investor, enterprise: enterprise, success: success, headers: headers);

  factory SingInModel.fromResponse(CustomHttpResponse response) {
    return SingInModel(
        investor: InvestorModel.fromMap(response.data["investor"]),
        enterprise: response.data["enterprise"],
        success: response.data["success"],
        headers: response.header);
  }
}
