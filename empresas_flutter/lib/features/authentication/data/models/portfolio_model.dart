import 'package:empresas_flutter/features/authentication/domain/entities/portfolio_entity.dart';

class PortfolioModel extends PortfolioEntity {
  PortfolioModel({
    required int enterprisesNumber,
    required List enterprises,
  }) : super(enterprisesNumber: enterprisesNumber, enterprises: enterprises);

  factory PortfolioModel.fromMap(Map<String, dynamic> map) {
    return PortfolioModel(
      enterprisesNumber: map["enterprises_number"],
      enterprises: map["enterprises"],
    );
  }
}
