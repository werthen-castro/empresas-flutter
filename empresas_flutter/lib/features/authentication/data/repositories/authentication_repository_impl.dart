import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source.dart';
import 'package:empresas_flutter/features/authentication/data/models/auth_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/sign_in_entity.dart';
import 'package:empresas_flutter/features/authentication/domain/repositories/authentication_repository.dart';

class AuthenticationRepositoryImpl implements AuthenticationRepository {
  final AuthenticationDataSource dataSource;

  AuthenticationRepositoryImpl(this.dataSource);

  @override
  Future<Either<Failure, SignInEntity>> signIn({required AuthModel auth}) async {
    try {
      final result = await dataSource.signIn(auth: auth);
      return Right(result);
    } on CustomServerException catch (e) {
      return Left(ServerFailure(
        errorMessage: e.messageError,
        statusCode: e.statusCode,
        timeout: e.timeout,
      ));
    }
  }
}
