import 'package:empresas_flutter/features/authentication/data/models/sign_in_model.dart';

import '../models/auth_model.dart';

abstract class AuthenticationDataSource {
  Future<SingInModel> signIn({required AuthModel auth});
}
