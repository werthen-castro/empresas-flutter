import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/http_client/custom_http_client.dart';
import 'package:empresas_flutter/core/http_client/custom_http_response.dart';
import 'package:empresas_flutter/core/utils/endpoints.dart';
import 'package:empresas_flutter/features/authentication/data/models/sign_in_model.dart';

import '../models/auth_model.dart';
import 'authentication_data_source.dart';

class AuthenticationDataSourceImpl implements AuthenticationDataSource {
  final CustomHttpClient _httpClient;

  AuthenticationDataSourceImpl(this._httpClient);
  @override
  Future<SingInModel> signIn({required AuthModel auth}) async {
    CustomHttpResponse response = await _httpClient.post(Endpoints.signIn(), body: auth.toJson());

    if (response.statusCode == 200) {
      return SingInModel.fromResponse(response);
    } else {
      return throw CustomServerException(
        messageError: response.statusMessage,
        apiError: response.apiError,
        timeout: response.timeout,
        statusCode: response.statusCode,
      );
    }
  }
}
