import 'package:empresas_flutter/core/utils/colors.dart';
import 'package:flutter/material.dart';

class CustomInpuText extends StatefulWidget {
  bool obscure;
  String? label;
  ScrollController? scroolController;
  TextEditingController controller;
  Widget? prefixIcon;
  Widget? suffixIcon;
  String? hint;
  Function()? onTap;
  Function(String? value)? onSubmitted;
  CustomInpuText({
    Key? key,
    this.obscure = false,
    this.label,
    this.scroolController,
    this.prefixIcon,
    this.suffixIcon,
    this.hint,
    this.onTap,
    this.onSubmitted,
    required this.controller,
  }) : super(key: key);

  @override
  State<CustomInpuText> createState() => _CustomInpuTextState();
}

class _CustomInpuTextState extends State<CustomInpuText> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        widget.label != null
            ? Row(
                children: [
                  Text(
                    widget.label!,
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              )
            : Container(),
        const SizedBox(
          height: 8,
        ),
        SizedBox(
          height: 48,
          child: TextField(
            obscureText: widget.obscure,
            controller: widget.controller,
            onTap: widget.onTap,
            onSubmitted: widget.onSubmitted,
            decoration: InputDecoration(
              prefixIcon: widget.prefixIcon,
              suffixIcon: widget.suffixIcon,
              filled: true,
              hintText: widget.hint,
              fillColor: AppColors.backgroundColorInput,
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.backgroundColorInput),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.backgroundColorInput),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.errorColor, width: 10),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
