import 'package:empresas_flutter/core/utils/colors.dart';
import 'package:flutter/material.dart';

class CustomButtomWidget extends StatefulWidget {
  final String title;
  final Function() onTap;
  CustomButtomWidget({required this.title, required this.onTap});

  @override
  _CustomButtomWidgetState createState() => _CustomButtomWidgetState();
}

class _CustomButtomWidgetState extends State<CustomButtomWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 48,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: AppColors.primaryColor,
          onPrimary: Colors.white,
        ),
        onPressed: widget.onTap,
        child: Text(
          widget.title.toUpperCase(),
          style: const TextStyle(fontSize: 16),
        ),
      ),
    );
  }
}
