import 'package:empresas_flutter/core/http_client/custom_http_client_impl.dart';
import 'package:empresas_flutter/core/utils/colors.dart';

import 'package:empresas_flutter/features/authentication/domain/usecases/sign_in_usecase.dart';
import 'package:empresas_flutter/features/authentication/presentation/controllers/sign_in_controller.dart';
import 'package:empresas_flutter/features/authentication/presentation/widgets/custom_button_widget.dart';
import 'package:empresas_flutter/features/authentication/presentation/widgets/custom_input_text.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/pages/search_enterprise_page.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class SignInPage extends StatefulWidget {
  AuthenticationUseCase? useCase;

  SignInPage({Key? key, this.useCase}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  late CustomHttpClientImpl _clientImpl;
  late String accessToken, client, uid;
  late ScrollController scroolController;
  late SignInController signInController;
  late AuthenticationUseCase _useCase;

  TextEditingController controllerEmail = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  @override
  void initState() {
    _useCase = widget.useCase ?? GetIt.instance.get<AuthenticationUseCase>();
    signInController = SignInController(_useCase);
    scroolController = ScrollController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: StreamBuilder<bool>(
            initialData: false,
            stream: signInController.isSelected,
            builder: (context, snapSelected) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: snapSelected.hasData && snapSelected.data! ? 150 : 250.0,
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: [
                        Positioned(
                          child: AnimatedContainer(
                            width: MediaQuery.of(context).size.width,
                            height: snapSelected.hasData && snapSelected.data! ? 150 : 250.0,
                            duration: const Duration(seconds: 1),
                            curve: Curves.decelerate,
                            child: Image.asset(
                              'assets/gradiente_horizontal.png',
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: snapSelected.hasData && snapSelected.data! ? 10 : 50.0,
                          child: Center(
                            child: Column(
                              children: [
                                Image.asset(
                                  'assets/logo.png',
                                  scale: 1.5,
                                ),
                                const SizedBox(
                                  height: 15,
                                ),
                                !(snapSelected.hasData && snapSelected.data!)
                                    ? const Text(
                                        'Seja bem vindo ao empresas!',
                                        style: TextStyle(color: Colors.white, fontSize: 20),
                                      )
                                    : Container()
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: StreamBuilder<bool>(
                        stream: signInController.isLoading,
                        builder: (context, snapLoadingt) {
                          return Center(
                            child: StreamBuilder<String?>(
                                stream: signInController.error,
                                builder: (context, snapError) {
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      CustomInpuText(
                                        label: 'Email',
                                        scroolController: scroolController,
                                        controller: controllerEmail,
                                        onTap: () {
                                          signInController.selected();
                                        },
                                      ),
                                      const SizedBox(
                                        height: 30,
                                      ),
                                      CustomInpuText(
                                        label: 'Senha',
                                        scroolController: scroolController,
                                        controller: controllerPassword,
                                        onTap: () {
                                          signInController.selected();
                                        },
                                      ),
                                      const SizedBox(
                                        height: 20,
                                      ),
                                      snapError.hasData
                                          ? Row(
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: [
                                                Text(
                                                  snapError.data.toString(),
                                                  style: TextStyle(fontSize: 15, color: AppColors.errorColor),
                                                ),
                                              ],
                                            )
                                          : Container(),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      CustomButtomWidget(
                                        onTap: () async {
                                          _onLoading();
                                          bool res = await signInController.signIn(
                                              password: controllerPassword.text, email: controllerEmail.text);
                                          Navigator.pop(context);
                                          if (res) {
                                            Navigator.push(context,
                                                MaterialPageRoute(builder: (context) => SearchEnterprisePage()));
                                          }
                                        },
                                        title: 'Entrar',
                                      ),
                                    ],
                                  );
                                }),
                          );
                        }),
                  ),
                ],
              );
            }));
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
            backgroundColor: Colors.transparent,
            child: Center(
                child: CircularProgressIndicator(
              color: AppColors.primaryColor,
            )));
      },
    );
  }
}
