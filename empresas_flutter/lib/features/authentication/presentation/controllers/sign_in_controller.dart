import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/core/utils/secure_storage.dart';
import 'package:empresas_flutter/features/authentication/domain/usecases/sign_in_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/pages/search_enterprise_page.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class SignInController {
  final _error = BehaviorSubject<String?>();
  Stream<String?> get error => _error.stream;

  final _isLoading = BehaviorSubject<bool>();
  Stream<bool> get isLoading => _isLoading.stream;

  final _isSelected = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isSelected => _isSelected.stream;

  final AuthenticationUseCase useCase;

  SignInController(this.useCase);

  selected() {
    _isSelected.add(true);
  }

  Future signIn({required String password, required String email}) async {
    _isLoading.add(true);
    _error.add(null);

    final result = await useCase(ListParams(password: password, email: email));

    result.fold((failureResponse) {
      if (failureResponse.timeout) {
        _error.add("Erro ao realizar o login, verifique seu conexão");
      } else {
        _error.add("Credenciais inválidas");
      }
    }, (response) async {
      SecureStorage secureStorage = SecureStorage();

      await secureStorage.setValue("access-token", response.headers["access-token"].first);
      await secureStorage.setValue("client", response.headers["client"].first);
      await secureStorage.setValue("uid", response.headers["uid"].first);
      _error.add(null);
    });

    _isLoading.add(false);
    return (_error.value == null);
  }
}
