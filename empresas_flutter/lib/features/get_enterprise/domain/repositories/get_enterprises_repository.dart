import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

abstract class GetEnterprisesRepository {
  Future<Either<Failure, List<EnterpriseModel>>> getEnterprises();
  Future<Either<Failure, List<EnterpriseModel>>> getEnterprisesSearchName({required String name});
  Future<Either<Failure, EnterpriseModel>> getEnterpriseForId({required int id});
}
