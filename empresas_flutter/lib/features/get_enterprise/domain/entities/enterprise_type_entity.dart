import 'package:equatable/equatable.dart';

class EnterpriseTypeEntity extends Equatable {
  late String enterpriseTypeName;
  late int id;
  EnterpriseTypeEntity({
    required this.enterpriseTypeName,
    required this.id,
  });

  @override
  List<Object?> get props => [enterpriseTypeName, id];
}
