import 'package:equatable/equatable.dart';

import 'enterprise_type_entity.dart';

class EnterpriseEntity extends Equatable {
  late String city;
  late String country;
  late String description;
  late String? emailEnterprise;
  late String enterpriseName;
  late String? facebook;
  late int id;
  late String? linkedin;
  late bool ownEnterprise;
  late int? ownShares;
  late String? phone;
  late String photo;
  late double sharePrice;
  int? shares;
  late String? twitter;
  late int value;
  late EnterpriseTypeEntity enterpriseType;

  EnterpriseEntity({
    required this.city,
    required this.country,
    required this.description,
    this.emailEnterprise,
    required this.enterpriseName,
    this.facebook,
    required this.id,
    this.linkedin,
    required this.ownEnterprise,
    this.ownShares,
    this.phone,
    required this.photo,
    required this.sharePrice,
    this.shares,
    this.twitter,
    required this.value,
    required this.enterpriseType,
  });

  @override
  List<Object?> get props => [
        city,
        country,
        description,
        emailEnterprise,
        enterpriseName,
        facebook,
        id,
        linkedin,
        ownEnterprise,
        ownShares,
        phone,
        photo,
        sharePrice,
        shares,
        twitter,
        value,
        enterpriseType
      ];
}
