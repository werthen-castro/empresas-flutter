import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

import '../repositories/get_enterprises_repository.dart';

class GetEnterprisesSearchNameUseCase implements Usecase<List<EnterpriseModel>, String> {
  final GetEnterprisesRepository repository;

  GetEnterprisesSearchNameUseCase(this.repository);

  @override
  Future<Either<Failure, List<EnterpriseModel>>> call(String name) async {
    return repository.getEnterprisesSearchName(name: name);
  }
}
