import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

import '../repositories/get_enterprises_repository.dart';

class GetEnterpriseForIdUseCase implements Usecase<EnterpriseModel, int> {
  final GetEnterprisesRepository repository;

  GetEnterpriseForIdUseCase(this.repository);

  @override
  Future<Either<Failure, EnterpriseModel>> call(int id) async {
    return repository.getEnterpriseForId(id: id);
  }
}
