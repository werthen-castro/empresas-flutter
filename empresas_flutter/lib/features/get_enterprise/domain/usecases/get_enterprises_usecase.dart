import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

import '../repositories/get_enterprises_repository.dart';

class GetEnterprisesUseCase implements Usecase<List<EnterpriseModel>, NoParams> {
  final GetEnterprisesRepository repository;

  GetEnterprisesUseCase(this.repository);

  @override
  Future<Either<Failure, List<EnterpriseModel>>> call(NoParams noParams) async {
    return repository.getEnterprises();
  }
}
