import 'package:empresas_flutter/core/utils/endpoints.dart';
import 'package:empresas_flutter/features/authentication/presentation/widgets/custom_button_widget.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_search_name_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/controller/get_enterprise_controller.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/widget/custom_header_widget.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/widget/imageview_widget.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'details_enterprise_page.dart';

class SearchEnterprisePage extends StatefulWidget {
  GetEnterprisesUseCase? getEnterprisesUseCase;
  GetEnterprisesSearchNameUseCase? getEnterprisesSearchNameUseCase;
  SearchEnterprisePage({Key? key, this.getEnterprisesUseCase, this.getEnterprisesSearchNameUseCase}) : super(key: key);

  @override
  State<SearchEnterprisePage> createState() => _SearchEnterprisePageState();
}

class _SearchEnterprisePageState extends State<SearchEnterprisePage> {
  late GetEnterprisesUseCase _getEnterprisesUseCase;
  late GetEnterprisesSearchNameUseCase _getEnterprisesSearchNameUseCase;
  late GetEnterpriseController controller;

  @override
  void initState() {
    _getEnterprisesUseCase = widget.getEnterprisesUseCase ?? GetIt.instance.get<GetEnterprisesUseCase>();
    _getEnterprisesSearchNameUseCase =
        widget.getEnterprisesSearchNameUseCase ?? GetIt.instance.get<GetEnterprisesSearchNameUseCase>();

    controller = GetEnterpriseController(_getEnterprisesUseCase, _getEnterprisesSearchNameUseCase);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<EnterpriseEntity>>(
        stream: controller.listEnterprises,
        builder: (context, snapshot) {
          return Scaffold(
              body: Column(
            children: [
              CustomHeaderWidget(
                controller: controller,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                child: Row(
                  children: [
                    Text(' ${snapshot.data?.length ?? 0} resultados encontrados'),
                  ],
                ),
              ),
              StreamBuilder<bool>(
                  stream: controller.isLoading,
                  builder: (context, snapshotLoading) {
                    return !(snapshotLoading.hasData && snapshotLoading.data!)
                        ? snapshot.hasData && snapshot.data!.length > 0
                            ? Expanded(
                                child: ListView.builder(
                                    padding: const EdgeInsets.all(8),
                                    itemCount: snapshot.data?.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      DetailsEnterprisePage(data: snapshot.data![index])));
                                        },
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                                          child: Stack(
                                            children: [
                                              SizedBox(
                                                  height: 150,
                                                  width: double.infinity,
                                                  child: ImageViewWidget(
                                                    path: Endpoints.getImage(snapshot.data![index].id),
                                                  )),
                                              Positioned(
                                                bottom: 5,
                                                left: 10,
                                                child: Center(
                                                  child: Text(
                                                    snapshot.data![index].enterpriseName,
                                                    style: const TextStyle(
                                                        fontSize: 25, color: Colors.white, fontWeight: FontWeight.bold),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    }),
                              )
                            : Expanded(
                                child: Center(
                                    child: Text(
                                'Nenhum resultado encontrado',
                                style: TextStyle(fontSize: 16),
                              )))
                        : Expanded(child: const Center(child: CircularProgressIndicator()));
                  })
            ],
          ));
        });
  }
}
