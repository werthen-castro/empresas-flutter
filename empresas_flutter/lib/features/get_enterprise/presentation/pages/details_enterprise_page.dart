import 'package:empresas_flutter/core/utils/colors.dart';
import 'package:empresas_flutter/core/utils/endpoints.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/widget/imageview_widget.dart';
import 'package:flutter/material.dart';

class DetailsEnterprisePage extends StatefulWidget {
  EnterpriseEntity data;
  DetailsEnterprisePage({Key? key, required this.data}) : super(key: key);

  @override
  State<DetailsEnterprisePage> createState() => _DetailsEnterprisePageState();
}

class _DetailsEnterprisePageState extends State<DetailsEnterprisePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              height: 20,
              width: 20,
              color: AppColors.backgroundColorInput,
              child: Icon(
                Icons.arrow_back,
                color: AppColors.primaryColor,
              ),
            ),
          ),
        ),
        title: Text(
          widget.data.enterpriseName,
          style: TextStyle(color: Colors.black, fontSize: 20),
          textAlign: TextAlign.center,
        ),
      ),
      body: Column(
        children: [
          ImageViewWidget(
            path: Endpoints.getImage(widget.data.id),
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Text(
              widget.data.description,
              style: TextStyle(fontSize: 16),
            ),
          )
        ],
      ),
    );
  }
}
