import 'dart:developer';

import 'package:empresas_flutter/core/http_client/custom_http_client_impl.dart';
import 'package:empresas_flutter/core/usecases/usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_search_name_usecase.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/usecases/get_enterprises_usecase.dart';
import 'package:rxdart/rxdart.dart';

class GetEnterpriseController {
  final _isSearch = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isSearch => _isSearch.stream;

  final _isLoading = BehaviorSubject<bool>.seeded(false);
  Stream<bool> get isLoading => _isLoading.stream;

  final _listEnterprises = BehaviorSubject<List<EnterpriseEntity>>();
  Stream<List<EnterpriseEntity>> get listEnterprises => _listEnterprises.stream;

  final GetEnterprisesUseCase getEnterprisesUseCase;
  final GetEnterprisesSearchNameUseCase getEnterprisesSearchNameUseCase;

  GetEnterpriseController(this.getEnterprisesUseCase, this.getEnterprisesSearchNameUseCase) {
    getEnterprises();
  }

  Future getEnterprises() async {
    _isLoading.add(true);
    _isSearch.add(false);

    final result = await getEnterprisesUseCase(NoParams());

    result.fold((failureResponse) {
      _listEnterprises.addError(failureResponse);
    }, (response) {
      _listEnterprises.add(response);
    });

    _isLoading.add(false);
  }

  Future search(String name) async {
    _isLoading.add(true);
    _isSearch.add(true);

    final result = await getEnterprisesSearchNameUseCase(name);

    result.fold((failureResponse) {
      _listEnterprises.addError(failureResponse);
    }, (response) {
      _listEnterprises.add(response);
    });

    _isLoading.add(false);
  }
}
