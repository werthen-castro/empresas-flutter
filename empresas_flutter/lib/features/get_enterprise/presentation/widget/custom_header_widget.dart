import 'package:empresas_flutter/features/authentication/presentation/widgets/custom_button_widget.dart';
import 'package:empresas_flutter/features/authentication/presentation/widgets/custom_input_text.dart';
import 'package:empresas_flutter/features/get_enterprise/presentation/controller/get_enterprise_controller.dart';
import 'package:flutter/material.dart';

class CustomHeaderWidget extends StatefulWidget {
  GetEnterpriseController controller;
  CustomHeaderWidget({Key? key, required this.controller}) : super(key: key);

  @override
  State<CustomHeaderWidget> createState() => _CustomHeaderWidgetState();
}

class _CustomHeaderWidgetState extends State<CustomHeaderWidget> {
  TextEditingController controllerSearch = TextEditingController();

  late GetEnterpriseController _controller;

  @override
  void initState() {
    _controller = widget.controller;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        initialData: false,
        stream: _controller.isSearch,
        builder: (context, snapshot) {
          return SizedBox(
            height: snapshot.hasData && snapshot.data! ? 130.0 : 230.0,
            child: Stack(
              children: [
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  child: Center(
                      child: AnimatedContainer(
                    width: MediaQuery.of(context).size.width,
                    height: snapshot.hasData && snapshot.data! ? 100.0 : 200.0,
                    duration: const Duration(seconds: 1),
                    curve: Curves.decelerate,
                    child: Image.asset(
                      'assets/gradiente_horizontal.png',
                      fit: BoxFit.cover,
                    ),
                  )),
                ),
                Positioned(
                  top: snapshot.hasData && snapshot.data! ? 70 : 170,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: CustomInpuText(
                      prefixIcon: Icon(Icons.search),
                      controller: controllerSearch,
                      hint: 'Pesquise por empresa',
                      onSubmitted: (value) {
                        _controller.search(controllerSearch.text);
                      },
                    ),
                  ),
                ),
              ],
            ),
          );
        });
  }
}
