import 'package:empresas_flutter/core/utils/colors.dart';
import 'package:flutter/material.dart';

class ImageViewWidget extends StatefulWidget {
  final String path;
  ImageViewWidget({required this.path});

  @override
  _ImageViewWidgetState createState() => _ImageViewWidgetState();
}

class _ImageViewWidgetState extends State<ImageViewWidget> {
  @override
  Widget build(BuildContext context) {
    return Image.network(
      widget.path,
      fit: BoxFit.cover,
      errorBuilder: (BuildContext context, Object exception, StackTrace? stackTrace) {
        return Center(
          child: Container(
            width: double.infinity,
            color: Colors.grey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    'Imagem indisponível',
                  ),
                ),
                Icon(
                  Icons.error,
                  size: 50,
                ),
              ],
            ),
          ),
        );
      },
      loadingBuilder: (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            color: AppColors.primaryColor,
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded / (loadingProgress.expectedTotalBytes!)
                : null,
          ),
        );
      },
    );
  }
}
