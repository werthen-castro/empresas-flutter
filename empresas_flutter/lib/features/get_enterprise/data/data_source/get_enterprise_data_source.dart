import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

abstract class GetEnterprisesDataSource {
  Future<List<EnterpriseModel>> getEnterprises();
  Future<List<EnterpriseModel>> getEnterprisesSearchName({required String name});
  Future<EnterpriseModel> getEnterpriseForId({required int id});
}
