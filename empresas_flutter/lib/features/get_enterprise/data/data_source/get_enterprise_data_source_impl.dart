import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/http_client/custom_http_client.dart';
import 'package:empresas_flutter/core/http_client/custom_http_response.dart';
import 'package:empresas_flutter/core/utils/endpoints.dart';
import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

import 'get_enterprise_data_source.dart';

class GetEnterprisesDataSourceImpl implements GetEnterprisesDataSource {
  final CustomHttpClient _httpClient;

  GetEnterprisesDataSourceImpl(this._httpClient);
  @override
  Future<List<EnterpriseModel>> getEnterprises() async {
    CustomHttpResponse response = await _httpClient.get(Endpoints.getEnterprises());

    if (response.statusCode == 200) {
      return List<EnterpriseModel>.from(response.data["enterprises"].map((model) => EnterpriseModel.fromMap(model)));
    } else {
      return throw CustomServerException(
        messageError: response.statusMessage,
        apiError: response.apiError,
        timeout: response.timeout,
        statusCode: response.statusCode,
      );
    }
  }

  @override
  Future<List<EnterpriseModel>> getEnterprisesSearchName({required String name}) async {
    CustomHttpResponse response = await _httpClient.get(Endpoints.getSearchEnterprises(name));

    if (response.statusCode == 200) {
      return List<EnterpriseModel>.from(response.data["enterprises"].map((model) => EnterpriseModel.fromMap(model)));
    } else {
      return throw CustomServerException(
        messageError: response.statusMessage,
        apiError: response.apiError,
        timeout: response.timeout,
        statusCode: response.statusCode,
      );
    }
  }

  @override
  Future<EnterpriseModel> getEnterpriseForId({required int id}) async {
    CustomHttpResponse response = await _httpClient.get(Endpoints.getEnterprisesForId(id));

    if (response.statusCode == 200) {
      return EnterpriseModel.fromMap(response.data);
    } else {
      return throw CustomServerException(
        messageError: response.statusMessage,
        apiError: response.apiError,
        timeout: response.timeout,
        statusCode: response.statusCode,
      );
    }
  }
}
