import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/features/get_enterprise/data/data_source/get_enterprise_data_source.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/repositories/get_enterprises_repository.dart';

import '../models/enterprise_model.dart';

class GetEnterprisesRepositoryImpl implements GetEnterprisesRepository {
  final GetEnterprisesDataSource dataSource;

  GetEnterprisesRepositoryImpl(this.dataSource);

  @override
  Future<Either<Failure, List<EnterpriseModel>>> getEnterprises() async {
    try {
      final result = await dataSource.getEnterprises();
      return Right(result);
    } on CustomServerException catch (e) {
      return Left(ServerFailure(
        errorMessage: e.messageError,
        statusCode: e.statusCode,
        timeout: e.timeout,
      ));
    }
  }

  @override
  Future<Either<Failure, List<EnterpriseModel>>> getEnterprisesSearchName({required String name}) async {
    try {
      final result = await dataSource.getEnterprisesSearchName(name: name);
      return Right(result);
    } on CustomServerException catch (e) {
      return Left(ServerFailure(
        errorMessage: e.messageError,
        statusCode: e.statusCode,
        timeout: e.timeout,
      ));
    }
  }

  @override
  Future<Either<Failure, EnterpriseModel>> getEnterpriseForId({required int id}) async {
    try {
      final result = await dataSource.getEnterpriseForId(id: id);
      return Right(result);
    } on CustomServerException catch (e) {
      return Left(ServerFailure(
        errorMessage: e.messageError,
        statusCode: e.statusCode,
        timeout: e.timeout,
      ));
    }
  }
}
