import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_type_entity.dart';

class EnterpriseTypeModel extends EnterpriseTypeEntity {
  EnterpriseTypeModel({
    required String enterpriseTypeName,
    required int id,
  }) : super(
          enterpriseTypeName: enterpriseTypeName,
          id: id,
        );

  factory EnterpriseTypeModel.fromMap(Map<String, dynamic> map) {
    return EnterpriseTypeModel(
      enterpriseTypeName: map["enterprise_type_name"],
      id: map["id"],
    );
  }
}
