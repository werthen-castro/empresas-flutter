import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_type_model.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_entity.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_type_entity.dart';

class EnterpriseModel extends EnterpriseEntity {
  EnterpriseModel(
      {required String city,
      required String country,
      required String description,
      required String enterpriseName,
      required int id,
      required bool ownEnterprise,
      int? ownShares,
      required String photo,
      required double sharePrice,
      int? shares,
      required int value,
      required EnterpriseTypeEntity enterpriseType,
      String? emailEnterprise,
      String? facebook,
      String? linkedin,
      String? twitter,
      String? phone})
      : super(
            city: city,
            country: country,
            description: description,
            enterpriseName: enterpriseName,
            id: id,
            ownEnterprise: ownEnterprise,
            ownShares: ownShares,
            photo: photo,
            sharePrice: sharePrice,
            shares: shares,
            value: value,
            enterpriseType: enterpriseType,
            emailEnterprise: emailEnterprise,
            facebook: facebook,
            linkedin: linkedin,
            twitter: twitter,
            phone: phone);

  factory EnterpriseModel.fromMap(Map<String, dynamic> map) {
    return EnterpriseModel(
      city: map["city"],
      country: map["country"],
      description: map["description"],
      emailEnterprise: map["email_enterprise"],
      enterpriseName: map["enterprise_name"],
      facebook: map["facebook"],
      id: map["id"],
      ownEnterprise: map["own_enterprise"],
      ownShares: map["own_shares"],
      phone: map["phone"],
      photo: map["photo"],
      sharePrice: map["share_price"].toDouble(),
      shares: map["shares"],
      value: map["value"],
      twitter: map["twitter"],
      enterpriseType: EnterpriseTypeModel.fromMap(map["enterprise_type"]),
    );
  }
}
