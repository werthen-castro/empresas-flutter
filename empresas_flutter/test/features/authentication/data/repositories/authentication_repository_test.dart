import 'package:dartz/dartz.dart';
import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/erros/failures.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source.dart';
import 'package:empresas_flutter/features/authentication/data/models/auth_model.dart';
import 'package:empresas_flutter/features/authentication/data/repositories/authentication_repository_impl.dart';
import 'package:empresas_flutter/features/authentication/domain/repositories/authentication_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/sign_in_model_mock.dart';

class MockAuthenticationDataSource extends Mock implements AuthenticationDataSource {}

void main() {
  late AuthenticationRepository repository;
  late AuthenticationDataSource dataSource;

  setUpAll(() {
    registerFallbackValue(AuthModel(email: "testeapple@ioasys.com.br", password: "12341234"));

    dataSource = MockAuthenticationDataSource();
    repository = AuthenticationRepositoryImpl(dataSource);
  });

  group('SignIn', () {
    test('should return  SignInEntity of the datasource', () async {
      // Arrange
      when(() => dataSource.signIn(auth: any(named: 'auth'))).thenAnswer((_) async => singInModelMock);

      // act
      final result = await repository.signIn(auth: AuthModel(email: "testeapple@ioasys.com.br", password: "12341234"));

      // Assert
      expect(result, Right(singInModelMock));
    });

    test('should return a server failure when call to datasource is unsecessful ( CustomServerException )', () async {
      // Arrange
      when(() => dataSource.signIn(auth: any(named: 'auth'))).thenThrow(const CustomServerException());

      // Act
      final result = await repository.signIn(auth: AuthModel(email: "testeapple@ioasys.com.br", password: "12341234"));

      //Assert
      expect(result, const Left(ServerFailure()));
    });
  });
}
