import 'dart:convert';

import 'package:empresas_flutter/features/authentication/data/models/investor_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/investor_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../mocks/investor_model_mock.dart';

void main() {
  test('should be a subclass of InvestorEntity', () {
    expect(investorModelMock, isA<InvestorEntity>());
  });

  test('should return a valid model', () {
    // Arrange
    final Map<String, dynamic> jsonMap = json.decode(investorJsonMock);

    // act
    final result = InvestorModel.fromMap(jsonMap);

    // Assert
    expect(result, investorModelMock);
  });
}
