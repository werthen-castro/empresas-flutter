import 'dart:convert';

import 'package:empresas_flutter/features/authentication/data/models/portfolio_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/portfolio_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../mocks/portfofio_model_mock.dart';

void main() {
  test('should be a subclass of PortfolioEntity', () {
    expect(portfolioModelMock, isA<PortfolioEntity>());
  });

  test('should return a valid model', () {
    // Arrange
    final Map<String, dynamic> jsonMap = json.decode(portfolioJsonMock);

    // act
    final result = PortfolioModel.fromMap(jsonMap);

    // Assert
    expect(result, portfolioModelMock);
  });
}
