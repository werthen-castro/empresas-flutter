import 'dart:convert';

import 'package:empresas_flutter/features/authentication/data/models/sign_in_model.dart';
import 'package:empresas_flutter/features/authentication/domain/entities/sign_in_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../mocks/sign_in_model_mock.dart';

void main() {
  test('should be a subclass of SingInEntity', () {
    expect(singInModelMock, isA<SignInEntity>());
  });
}
