import 'dart:convert';

import 'package:empresas_flutter/core/erros/custom_server_exceptions.dart';
import 'package:empresas_flutter/core/http_client/custom_http_client.dart';
import 'package:empresas_flutter/core/http_client/custom_http_response.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source.dart';
import 'package:empresas_flutter/features/authentication/data/data_source/authentication_data_source_impl.dart';
import 'package:empresas_flutter/features/authentication/data/models/auth_model.dart';
import 'package:empresas_flutter/features/authentication/data/models/sign_in_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../../../mocks/sign_in_model_mock.dart';

class MockCustomHttpClient extends Mock implements CustomHttpClient {}

void main() {
  late AuthenticationDataSource datasource;
  late CustomHttpClient httpClient;

  setUp(() {
    httpClient = MockCustomHttpClient();
    datasource = AuthenticationDataSourceImpl(httpClient);
  });

  group('SignIn - ', () {
    const String urlExpectedSignIn = "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in";

    void sucessMockSignIn() {
      when(() => httpClient.post(any(), body: any(named: 'body')))
          .thenAnswer((_) async => CustomHttpResponse(data: json.decode(data), statusCode: 200));
    }

    test('should call the get method with correct url', () async {
      // Arrange
      sucessMockSignIn();

      // Act
      await datasource.signIn(auth: AuthModel(email: "testeapple@ioasys.com.br", password: "12341234"));

      // Assert
      verify(() =>
              httpClient.post(urlExpectedSignIn, body: {"email": "testeapple@ioasys.com.br", "password": "12341234"}))
          .called(1);
    });

    test('should return a SigInModel when a sucessfull', () async {
      // Arrange
      sucessMockSignIn();

      // Act
      SingInModel result =
          await datasource.signIn(auth: AuthModel(email: "testeapple@ioasys.com.br", password: "12341234"));

      // Assert
      expect(result, singInModelMock);
    });

    test("Error HTTP - should throw a CustomServerException when is uncessful, Unauthorized", () async {
      // Arrange
      when(() => httpClient.post(any(), body: any(named: 'body'))).thenAnswer((_) async => CustomHttpResponse(
          statusMessage: "Invalid API key: You must be granted a valid key",
          statusCode: 401,
          apiError: "Invalid login credentials. Please try again."));

      // Act
      var result = datasource.signIn(auth: AuthModel(email: "testeapple@ioasys.com.br", password: "123412349"));

      // Assert
      expect(
          () => result,
          throwsA(predicate((e) =>
              e is CustomServerException &&
              e.messageError == 'Invalid API key: You must be granted a valid key' &&
              e.apiError == "Invalid login credentials. Please try again.")));
    });
  });
}
