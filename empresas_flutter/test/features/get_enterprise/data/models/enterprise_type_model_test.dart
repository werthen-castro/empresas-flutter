import 'dart:convert';

import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_type_model.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_type_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../mocks/enterprise_type_model_mock.dart';

void main() {
  test('should be a subclass of EnterpriseTypeEntity', () {
    expect(enterpriseTypeModelMock, isA<EnterpriseTypeEntity>());
  });

  test('should return a valid model', () {
    // Arrange
    final Map<String, dynamic> jsonMap = json.decode(enterpriseTypeJsonMock);

    // act
    final result = EnterpriseTypeModel.fromMap(jsonMap);

    // Assert
    expect(result, enterpriseTypeModelMock);
  });
}
