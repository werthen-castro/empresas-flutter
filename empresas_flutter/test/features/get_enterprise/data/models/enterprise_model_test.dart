import 'dart:convert';

import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';
import 'package:empresas_flutter/features/get_enterprise/domain/entities/enterprise_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../mocks/enterprise_model_mock.dart';

void main() {
  test('should be a subclass of EnterpriseEntity', () {
    expect(enterpriseModelMock, isA<EnterpriseEntity>());
  });

  test('should return a valid model', () {
    // Arrange
    final Map<String, dynamic> jsonMap = json.decode(entrepriseJsonMock);

    // act
    final result = EnterpriseModel.fromMap(jsonMap);

    // Assert
    expect(result, enterpriseModelMock);
  });
}
