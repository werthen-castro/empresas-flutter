import 'package:empresas_flutter/features/authentication/data/models/investor_model.dart';

import 'portfofio_model_mock.dart';

InvestorModel investorModelMock = InvestorModel(
  id: 1,
  investorName: "Teste Apple",
  city: "BH",
  country: "Brasil",
  balance: 1000000.0,
  photo: null,
  portfolio: portfolioModelMock,
  portfolioValue: 1000000.0,
  firstAccess: false,
  superAngel: false,
  email: "testeapple@ioasys.com.br",
);

String investorJsonMock = """   
     {
        "id": 1,
        "investor_name": "Teste Apple",
        "email": "testeapple@ioasys.com.br",
        "city": "BH",
        "country": "Brasil",
        "balance": 1000000.0,
        "photo": null,
        "portfolio": {
            "enterprises_number": 0,
            "enterprises": []
        },
        "portfolio_value": 1000000.0,
        "first_access": false,
        "super_angel": false
    }""";
