import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_model.dart';

import 'enterprise_type_model_mock.dart';

String entrepriseJsonMock = ''' {
    "city": "Bristol",
    "country": "UK",
    "description": "FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.",
    "email_enterprise": null,
    "enterprise_name": "Fluoretiq Limited",
    "enterprise_type": {
        "enterprise_type_name": "Health",
        "id": 3
    },
    "facebook": null,
    "id": 1,
    "linkedin": null,
    "own_enterprise": false,
    "own_shares": 0,
    "phone": null,
    "photo": "/uploads/enterprise/photo/1/240.jpeg",
    "share_price": 10000,
    "shares": 100,
    "twitter": null,
    "value": 0
}''';

EnterpriseModel enterpriseModelMock = EnterpriseModel(
    city: 'Bristol',
    country: 'UK',
    description:
        'FluoretiQ is a Bristol based medtech start-up developing diagnostic technology to enable bacteria identification within the average consultation window, so that patients can get the right anti-biotics from the start.',
    enterpriseName: "Fluoretiq Limited",
    enterpriseType: enterpriseTypeModelMock,
    id: 1,
    ownEnterprise: false,
    ownShares: 0,
    photo: '/uploads/enterprise/photo/1/240.jpeg',
    sharePrice: 10000,
    shares: 100,
    value: 0);
