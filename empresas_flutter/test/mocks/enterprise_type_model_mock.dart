import 'package:empresas_flutter/features/get_enterprise/data/models/enterprise_type_model.dart';

String enterpriseTypeJsonMock = '''{
        "enterprise_type_name": "Health",
        "id": 3
    }''';

EnterpriseTypeModel enterpriseTypeModelMock = EnterpriseTypeModel(enterpriseTypeName: "Health", id: 3);
