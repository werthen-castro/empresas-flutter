import 'package:empresas_flutter/features/authentication/data/models/sign_in_model.dart';

import 'investor_model_mock.dart';

SingInModel singInModelMock = SingInModel(enterprise: null, investor: investorModelMock, success: true, headers: {});

String data = '''{
    "investor": {
        "id": 1,
        "investor_name": "Teste Apple",
        "email": "testeapple@ioasys.com.br",
        "city": "BH",
        "country": "Brasil",
        "balance": 1000000.0,
        "photo": null,
        "portfolio": {
            "enterprises_number": 0,
            "enterprises": []
        },
        "portfolio_value": 1000000.0,
        "first_access": false,
        "super_angel": false
    },
    "enterprise": null,
    "success": true
} ''';
