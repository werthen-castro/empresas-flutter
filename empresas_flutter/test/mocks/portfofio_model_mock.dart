import 'package:empresas_flutter/features/authentication/data/models/portfolio_model.dart';

PortfolioModel portfolioModelMock = PortfolioModel(enterprisesNumber: 0, enterprises: const []);

String portfolioJsonMock = """ {
          "enterprises_number": 0,
          "enterprises": []
        } """;
